#!/usr/bin/env python3

import math
import time
import json
import logging
import pyliskcrypt

LOG = logging.getLogger(__name__)

class init():
    """
    1. Grab info from payout aggregate
    2. Create transactions based on info gathered
    3.  Transmit transactions
        a. if online send to api endpoint
        b. if offline generate transactions and save

    Need to be able to zero out the accounts that match the gathered
    """

    def __init__(self, constants, lsk_utils):
        """
        """
        self.constants = constants
        self.lsk_utils = lsk_utils

    def gather_accounts(self):
        """
        """
        self.accounts = self.lsk_utils.db_gather_aggregate(self.constants.min_payout)

    def generate_offline(self, secret1, secret2, accounts, timestamp, txfee):
        """
        offline transaction generator
        """
        transactions = []

        pubk, privk, pubk2, privk2 = pyliskcrypt.get_pub_priv_key(secret1, secret2)


        for payout_account in accounts:

            resp = pyliskcrypt.gen_tx(pubk,
                                      privk,
                                      pubk2,
                                      privk2,
                                      payout_account['rewards'],
                                      payout_account['address'],
                                      timestamp,
                                      txfee)

            transactions.append(resp)

        # Save the transaction object to file
        step = 24
        tx_new = []
        if len(transactions) <= 25:
            with open('transaction{}.json'.format(step), 'w') as jfh:
                json.dump({"transactions":transactions}, jfh)
                return

        for idx, transaction in enumerate(transactions):
            tx_new.append(transaction)
            print(idx)
            if idx == step:
                with open('transaction{}.json'.format(step), 'w') as jfh:
                    json.dump({"transactions":tx_new}, jfh)
                step += 25
                tx_new = []

    def broadcast_offline(self):
        """
        offline transaction broadcaster
        """
        pass

    def broadcast_online(self, secret1, secret2, interval):
        """
        use api transactions sending the passphrase
        to the endpoint (legacy)

        need to write to db here per tx for the IDs
        """
        address = self.constants.url + self.constants.transactions_uri

        payload = {
            'amount': 0,
            'secret': secret1,
            'recipientId': None
        }

        if secret2:
            payload['secondSecret'] = secret2

        responses = []

        for voter in self.accounts:

            payload['amount'] = int(voter['rewards'] * math.pow(10, 8))
            payload['recipientId'] = voter['address']

            resp = self.lsk_utils.api_put(address,
                                          payload,
                                          self.constants.headers)

            # If the tx was broadcasted and got an ID update the db
            # else leave intact for the next run and notify?
            # txid, address, amount, success, error
            self.lsk_utils.db_update_transactions(resp.get('transactionId', 0),
                                                  payload['recipientId'],
                                                  payload['amount'],
                                                  resp.get('success', False),
                                                  resp.get('error', 0),
                                                  True)

            time.sleep(interval)

            responses.append(resp)

        return responses
