#!/usr/bin/env python3

import time
import yaml
import logging
import requests
import sqlite3

LOG = logging.getLogger(__name__)

class init:
    """
    """

    def api_get(self, url):
        """
        Helper method for api get requests
        Args:
            url      (str ): url to request
        Returns:
            response (dict): dictionary of the api response
            response (None): return on exception
        """
        try:
            response = requests.get(url).json()
            return response
        except:
            LOG.error("api get failed on {}".format(url))
            return None

    def api_put(self, url, data, headers):
        """
        Helper method for api get requests
        Args:
            url      (str ): url to request
        Returns:
            response (dict): dictionary of the api response
            response (None): return on exception
        """
        LOG.debug(url)
        try:
            response = requests.put(url, json=data).json()
            LOG.debug(response)
            return response
        except:
            err = "api put failed on {}".format(url)
            LOG.error(err)
            return {'error': err, 'success': False}

    def config_update(self, filedir, updates):
        """
        """

        try:

            with open(filedir, 'w') as ymlfh:

                yaml.safe_dump(updates, ymlfh,
                               allow_unicode=True,
                               default_flow_style=False)

                return True

        except IOError:

            return False

    def open_db(self, db_file):
        """
        Open connection to the sqlite database
        """
        self.conn = sqlite3.connect(db_file)
        self.cursor = self.conn.cursor()

    def db_select(self, statement):
        """
        """
        try:
            self.cursor.execute(statement)
        except Exception as err:
            return str(err)

        if self.cursor.description is None:
            return None

        column_names = [desc[0] for desc in self.cursor.description]

        results = []

        for row in self.cursor:
            new_row = [x.tobytes().decode() if isinstance(x, memoryview)
                       else x for x in row]
            results.append(dict(zip(column_names, new_row)))

        return results

    def db_update_transactions(self, txid, address, amount, success, message, upd):
        """
        """
        # Create transactions table if not exists
        create_if_not_exists = """CREATE TABLE IF NOT EXISTS transactions(
                txid TEXT,
                address TEXT,
                amount INT,
                success int,
                message text,
                timestamp DATETIME DEFAULT CURRENT_TIMESTAMP)"""

        self.cursor.execute(create_if_not_exists)
        self.conn.commit()

        if upd:
            self.cursor.execute('''
                UPDATE transactions SET txid = ?, success = ?, message = ?
                WHERE address = ? AND txid = 0''',
                (txid, success, message, address))
        else:
            self.cursor.execute('''
                INSERT INTO transactions(txid, address, amount, success, message)
                VALUES(?,?,?,?,?)''',
                (txid, address, amount, success, message))

        self.conn.commit()

    def db_update_payouts(self, payouts, dry_run):
        """
        """
        create_if_not_exists = """CREATE TABLE IF NOT EXISTS payout_details(
            address text,
            rewards int,
            dry_run int,
            timestamp DATETIME DEFAULT CURRENT_TIMESTAMP)"""

        self.cursor.execute(create_if_not_exists)
        self.conn.commit()

        create_if_not_exists = """CREATE TABLE IF NOT EXISTS payout_aggregate(
            address text PRIMARY KEY,
            rewards int,
            dry_run int)"""

        self.cursor.execute(create_if_not_exists)
        self.conn.commit()

        for address, amount in payouts.items():

            self.cursor.execute('''
                INSERT INTO payout_details(address, rewards,
                dry_run) VALUES(?,?,?)''',
                (address, amount, dry_run))

            if dry_run is False:
                self.cursor.execute('''
                    REPLACE INTO payout_aggregate(address, rewards, dry_run)
                    VALUES(?,
                    COALESCE((SELECT rewards FROM payout_aggregate WHERE address = ?), 0) + ?,
                    ?)''', (address, address, amount, dry_run))

        self.conn.commit()

    def db_update_summary(self, summary, dry_run):
        """
        """
        create_if_not_exists = """
            CREATE TABLE IF NOT EXISTS payout_summary(
                timeframe TEXT,
                delegate TEXT,
                weight INT,
                weight_filtered INT,
                vote_count INT,
                Vote_count_filtered INT,
                forged INT,
                rewards INT,
                fees INT,
                percentage INT,
                forged_to_give INT,
                forged_blk_count INT,
                dry_run INT,
                timestamp DATETIME DEFAULT CURRENT_TIMESTAMP)"""

        self.cursor.execute(create_if_not_exists)
        self.conn.commit()

        self.cursor.execute('''
            INSERT INTO payout_summary(
                timeframe,
                delegate,
                weight,
                weight_filtered,
                vote_count,
                Vote_count_filtered,
                forged,
                rewards,
                fees,
                percentage,
                forged_to_give,
                forged_blk_count,
                dry_run)
            VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)''',
                (summary.get('Timeframe'),
                 summary.get('Delegate pubkey'),
                 summary.get('Weight'),
                 summary.get('Weight Filtered'),
                 summary.get('VoterCount'),
                 summary.get('VoterCount Filtered'),
                 summary.get('Forged'),
                 summary.get('Rewards'),
                 summary.get('Fees'),
                 summary.get('Percentage'),
                 summary.get('Forged to give'),
                 summary.get('Forged BLK Count'),
                 dry_run))

        self.conn.commit()

    def db_alter_aggregate(self):
        """
        This will alter the aggregate table after a payout
        so it resets the payout amount
        """
        now = int(time.time())

        alter_table = '''
            ALTER TABLE payout_aggregate
            RENAME TO payout_aggregate_{}'''.format(now)

        self.cursor.execute(alter_table)
        self.conn.commit()

    def db_gather_aggregate(self, min_payout):
        """
        #TODO need to change the rewards amount to something
        that makes sense from constants
        """
        statement = 'SELECT * FROM payout_aggregate WHERE rewards >= {}'.format(min_payout)
        return self.db_select(statement)

    def db_clear_payout_accounts(self, accounts):
        """
        Zeroize accounts that are scheduled for a payout
        """
        for acct in accounts:
            LOG.debug(acct)
            amount = float(abs(acct['rewards'] - round(acct['rewards'], 8)))
            self.cursor.execute('''
                    UPDATE payout_aggregate SET rewards = ?
                    WHERE address = ?''', (amount, acct['address']))

        self.conn.commit()

