#!/usr/bin/env python3

import logging

LOG = logging.getLogger(__name__)

class init:
    """
    """
    def __init__(self, constants, utils):
        """
        """
        self.constants = constants
        self.utils = utils

    def get_forged(self):
        """
        {'success': True,
        'fees': '187065844416',
        'rewards': '16780500000000',
        'forged': '16967565844416'}
        """
        # 
        url = self.constants.url + self.constants.getforgedby_account
        url = url.format(self.constants.pubkey, int(self.constants.timestamp))
        LOG.debug(url)

        # 
        resp = self.utils.api_get(url)

        #
        self.forged = resp

    def get_voters(self):
        """
        'accounts':[{'username': None,
            'address': '13638082544609590678L',
            'publicKey': 'a1b192bc09949e24ad27ad7f045ed0ae0ee710c651fcbdcfdfb05ef8b2d63eec',
            'balance': '104181514988921'}]
        """
        # 
        url = self.constants.url + self.constants.getaccount_voters
        url = url.format(self.constants.pubkey)
        # 
        resp = self.utils.api_get(url)

        #
        self.voters = resp

