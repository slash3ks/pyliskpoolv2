#!/usr/bin/env python3

import os
import sys
import yaml
import datetime

class init:
    """
    Constant variables
    """
    def __init__(self,
                 timestamp,
                 pubkey,
                 percentage,
                 config_file,
                 url,
                 db,
                 exception_list,
                 hour_schedule,
                 txfee,
                 genesis_timestamp):

        self.timestamp = timestamp
        self.last_run = timestamp
        self.pubkey = pubkey
        self.percentage = percentage 
        self.url = url
        self.db = db
        self.txfee = txfee
        self.genesis_timestamp = genesis_timestamp

        cwd = os.getcwd()

        try:

            if not config_file:

                config_file = cwd + '/config.yaml'

            with open(config_file) as fh:

                self.data = yaml.safe_load(fh)

        except IOError:
            raise
            sys.exit("Cannot open configuration file in constants.py")

        try:

            if not exception_list:

                exception_list = cwd + '/exception_list.yaml'

            with open(exception_list) as el:

                self.ignore = yaml.safe_load(el)

        except IOError:
            sys.exit("Cannot open exception list")

        if not self.timestamp:
            self.timestamp = (datetime.datetime.now() - datetime.timedelta(hours=1)).timestamp()

        if not self.db:
            self.db_file = cwd + '/pyliskpool.sqlite'
        else:
            self.db_file = db

        if not self.url:
            self.url = self.data.get('url')

        self.min_payout = self.data.get('min_payout')

        # Forger pubkey
        if not self.pubkey:
            self.pubkey = self.data.get('pubkey')

        # Sharing percentage (int)
        if not self.percentage:
            self.percentage = self.data.get('percentage')

        # JSON headers
        self.headers = {"Content-Type": "application/json"}

        # API endpoints
        self.getforgedby_account = '/api/delegates/forging/getForgedByAccount?generatorPublicKey={}&start={}'
        self.getaccount_voters = '/api/delegates/voters?publicKey={}'
        self.transactions_uri = '/api/transactions'

