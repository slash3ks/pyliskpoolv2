#!/usr/bin/env python3
"""
"""

import sys
import time
import datetime
import math
import getpass
import argparse
import logging
import json
import tx
import poller
import utils
import constants
import accounting

LOG = logging.getLogger(__name__)

def payout(lsk_utils, constants, interval, second_phrase, online, offline, dry_run):
    """
    """

    # Open a DB connection
    lsk_utils.open_db(constants.db_file)

    # Init transactions obj
    transactions = tx.init(constants, lsk_utils)

    # Gather the accounts that meet the min criteria
    transactions.gather_accounts()

    if dry_run:
        return transactions.accounts

    # Add transactions to the tx table
    for txdata in transactions.accounts:

        lsk_utils.db_update_transactions(0,
                                         txdata.get('address', 0),
                                         txdata.get('rewards'),
                                         False,
                                         'Queued',
                                         False)

    date = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d-%H:%M:%S-')

    # Before sending save to log to have a copy in case something blows up
    with open('{}transactions.json'.format(date), 'w') as json_fh:
        json.dump({"transactions": transactions.accounts}, json_fh)

    # Clear the accounts that have met the min criteria
    lsk_utils.db_clear_payout_accounts(transactions.accounts)

    # Need the grab the passphrases here
    try:
        secret1 = getpass.getpass("First passphrase: ")
        secret1_1 = getpass.getpass("Verifying. Please enter it again: ")
        if secret1 != secret1_1:
            sys.exit("First pass does not match. please try again.")
    except KeyboardInterrupt:
        sys.exit("Operation aborted. No phrase given")

    if second_phrase:
        try:
            secret2 = getpass.getpass("Second passphrase: ")
            secret2_1 = getpass.getpass("Verifying. Please enter it again: ")
            if secret2 != secret2_1:
                sys.exit("Second pass does not match. please try again.")
        except KeyboardInterrupt:
            sys.exit("Operation aborted. No phrase given")
    else:
        secret2 = None

    # Send the transactions that are queued up
    if online:
        resp = transactions.broadcast_online(secret1, secret2, interval)
    elif offline:
        resp = transactions.generate_offline(secret1,
                                             secret2,
                                             transactions.accounts,
                                             constants.genesis_timestamp,
                                             constants.txfee)
    else:
        resp = []

    return resp

def summarize(acct):
    """
    """
    from_date = datetime.datetime.fromtimestamp(int(acct.constants.timestamp))
    to_date = datetime.datetime.now()
    summary = {
        'Timeframe': '{} to {}'.format(from_date, to_date),
        'Delegate pubkey': acct.constants.pubkey,
        'Weight': acct.weight_unfiltered,
        'Weight Filtered': acct.total_vote_weight,
        'Forged': int(acct.forged.get('forged')) / math.pow(10, 8),
        'Rewards': acct.rewards,
        'Fees': int(acct.forged.get('fees')) / math.pow(10, 8),
        'Percentage': acct.constants.percentage,
        'Forged to give': acct.forged_to_give,
        'Forged BLK Count': acct.forged.get('count'),
        'VoterCount': acct.voters_count_unfiltered,
        'VoterCount Filtered': acct.voter_count
        }

    return summary

def summary_pretty_print(summary):
    """
    """
    print('Timeframe:         {}'.format(summary['Timeframe']))
    print('Delegate pubkey:   {}'.format(summary['Delegate pubkey']))
    print('Weight:            {}'.format(summary['Weight']))
    print('Weight Filtered:   {}'.format(summary['Weight Filtered']))
    print('VoteCount:         {}'.format(summary['VoterCount']))
    print('VoteCount filtered:{}'.format(summary['VoterCount Filtered']))
    print('Forged:            {}'.format(summary['Forged']))
    print('Rewards:           {}'.format(summary['Rewards']))
    print('Fees:              {}'.format(summary['Fees']))
    print('Percentage:        {}'.format(summary['Percentage']))
    print('Forged to give:    {}'.format(summary['Forged to give']))
    print('Forged BLK Count:  {}'.format(summary['Forged BLK Count']))

def main(args):
    """
    sub-main brah
    """

    # Init utils
    lsk_utils = utils.init()

    # Init constants
    constants_obj = constants.init(args.timestamp,
                                   args.pubkey,
                                   args.percentage,
                                   args.config,
                                   args.url,
                                   args.db,
                                   args.exception_list,
                                   args.hour_schedule,
                                   args.txfee,
                                   args.genesis_timestamp)


    # Payout options online/offline
    if args.payout:

        resp = payout(lsk_utils,
                      constants_obj,
                      args.interval,
                      args.second_phrase,
                      args.online,
                      args.offline,
                      args.dry_run)

        for item in resp:
            print(item)

        if args.dry_run:
            total = sum([x['rewards'] for x in resp])
            print(total)

        sys.exit()

    # Initialize the poller
    poller_obj = poller.init(constants_obj, lsk_utils)

    # Get voters of delegate
    poller_obj.get_voters()

    # Get forged amount for selected timeframe
    poller_obj.get_forged()

    # initialize the accounting class
    acct = accounting.init(constants_obj, poller_obj.voters, poller_obj.forged)

    # Summary does not write anything to logs or DB
    acct.summary()

    # Create a summary
    summary = summarize(acct)

    if args.summary is True:
        summary_pretty_print(summary)
        sys.exit()

    # after getting the rewards need to write the amounts to the DB
    #lsk_utils.config_update('./payouts.yaml', {constants_obj.last_run: acct.payouts})

    # Init the db connection
    lsk_utils.open_db(constants_obj.db_file)

    # Update the database with summary information
    lsk_utils.db_update_summary(summary, args.dry_run)

    # Update the database with the payout information
    lsk_utils.db_update_payouts(acct.payouts, args.dry_run)

def usage():
    """
    """

    return '''
    python3 pyliskpool

    Summary View to stdout (no logs or db write)
    python3 pyliskpool --summary --config ~/pyliskpoolv2/config.yaml -e ~/pyliskpoolv2/exception_list.yaml --db ~/pyliskpoolv2/pyliskpool.sqlite

    python3 pyliskpool --payout --config ~/pyliskpoolv2/config.yaml -e ~/pyliskpoolv2/exception_list.yaml --db ~/pyliskpoolv2/pyliskpool.sqlite
    '''

if __name__ == '__main__':

    PARSER = argparse.ArgumentParser(description='LISK Pool', usage=usage())

    PARSER.add_argument('--debug', dest='debug',
                        action='store_true', help='')

    PARSER.add_argument('--summary', dest='summary',
                        action='store_true', help='')

    PARSER.add_argument('--payout', dest='payout',
                        action='store_true', help='')

    PARSER.add_argument('--offline', dest='offline',
                        action='store_true', help='')

    PARSER.add_argument('--online', dest='online',
                        action='store_true', help='')

    PARSER.add_argument('--dry-run', dest='dry_run',
                        action='store_true', default=False,
                        help='Dry run flag')

    PARSER.add_argument('-p', '--public-key', dest='pubkey',
                        action='store', help='Delegate Public key')

    PARSER.add_argument('--percentage', dest='percentage', type=int,
                        action='store', help='Reward percentage')

    PARSER.add_argument('-t', '--timestamp', dest='timestamp',
                        action='store', help='Timestamp to start')

    PARSER.add_argument('-gt', '--genesis-timestamp', dest='genesis_timestamp',
                        action='store', type=int, default=1464109200,
                        help='Timestamp to start')

    PARSER.add_argument('--tx-fee', dest='txfee', action='store',
                        type=int, default=10000000,
                        help='Default fee for type 0 transaction')

    PARSER.add_argument('-i', '--interval', dest='interval', type=float,
                        default=0.5, action='store',
                        help='How often to send transactions in seconds')

    PARSER.add_argument('-c', '--config', dest='config', action='store',
                        help='Config file.')

    PARSER.add_argument('-d', '--db', dest='db', action='store',
                        help='Database file')

    PARSER.add_argument('-e', '--exception-list', dest='exception_list',
                        action='store', help='Exception list file')

    # How often to calculate and add to DB
    # This value must match the schedule in cron
    PARSER.add_argument('--hour-schedule', dest='hour_schedule',
                        type=int, default=1, action='store',
                        help='')

    PARSER.add_argument('-u', '--url', dest='url', action='store',
                        help='')

    PARSER.add_argument('--second-pass', dest='second_phrase',
                        action='store_true', default=False,
                        help='Second passphrase flag')

    ARGS = PARSER.parse_args()

    logging.getLogger("requests").setLevel(logging.WARNING)
    if ARGS.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    main(ARGS)
