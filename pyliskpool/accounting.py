#!/usr/bin/env python3

import math
import datetime
import logging

LOG = logging.getLogger(__name__)

class init():
    """
    """

    def __init__(self, constants, voters, forged):
        """
        """
        self.constants = constants
        self.voters = voters
        self.forged = forged

    def filter_voters(self):
        """
        """
        processed_list = []
        new_voter_list = []

        self.weight_unfiltered = sum([float(x['balance']) / math.pow(10, 8)
                                      for x in self.voters['accounts']])

        self.voters_count_unfiltered = len(self.voters['accounts'])

        for x in self.voters['accounts']:

            if x['address'] not in self.constants.ignore['addresses'] and \
                    x['publicKey'] not in self.constants.ignore['addresses'] and \
                    x['username'] not in self.constants.ignore['addresses']:

                processed_list.append(x)

        total_weight = sum([float(x['balance']) / math.pow(10, 8)
                            for x in processed_list])

        self.total_vote_weight = total_weight
        self.voter_count = len(processed_list)
        self.voter_processed_list = processed_list

    def process_voter_rewards(self):
        """
        """
        payouts = {}

        self.rewards =  float(self.forged['rewards']) / math.pow(10, 8)
        self.forged_to_give = self.rewards * self.constants.percentage / 100

        for voter in self.voter_processed_list:

            address = voter['address']
            balance = float(voter['balance'])
            amount = balance / math.pow(10, 8)
            payout = (amount * self.forged_to_give) / self.total_vote_weight

            payouts[voter['address']] = payout

        self.payouts = payouts

    def summary(self):
        """
        """
        self.filter_voters()
        self.process_voter_rewards()
