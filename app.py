#!/usr/bin/env python3
"""

"""
#TODO refresh when db update?
#TODO read template from file

import sys
import random
import argparse
from flask import Flask, abort, request, jsonify
from flask import render_template
from flask_restful import Resource, Api
from sqlalchemy import create_engine
from flasgger import Swagger, LazyString
from flasgger.utils import swag_from

#
class PyLskPool(Flask):
    def __init__(self, *args, **kwargs):
            super(PyLskPool, self).__init__(*args, **kwargs)

            self.payouts = []
            self.payouts_summary = []
            self.payouts_aggregate = []
            self.transactions = []
            self.transactions_details = []

#
app = PyLskPool(__name__)
api = Api(app)

swagger_config = {
    "headers": [],
    "specs": [
        {
            "endpoint": 'apispec_1',
            "route": '/apispec_1.json',
            "rule_filter": lambda rule: True,  # all in
            "model_filter": lambda tag: True,  # all in
        }
    ],
    "static_url_path": "/flasgger_static",
    # "static_folder": "static",  # must be set by user
    "swagger_ui": True,
    "specs_route": "/apidocs/"
}

template = {
    "swagger": "2.0",
    "info": {
        'title': 'slasheks delegate LSK Pool API',
        'version': '0.0.1',
        'description': 'Pool transactions and other information',
        "contact": {
            "responsibleOrganization": "ME",
            "responsibleDeveloper": "Me",
            "email": "me@me.com",
            "url": "www.me.com",
        }
    },
    #"host": "mysite.com",  # overrides localhost:500
    #"basePath": "/api",  # base bash for blueprint registration
    "schemes": [ "http", "https" ],
    "operationId": "getmyData"
}

swagger = Swagger(app, config=swagger_config, template=template)

# Create a engine for connecting to SQLite3
e = create_engine('sqlite:///pyliskpool.sqlite')

def get_payouts():
    """
    Run at startup and add to app context the transactions
    """
    conn = e.connect()
    try:
        query = conn.execute("SELECT * FROM payout_details")
    except:
        sys.exit('Payout details table error')
    column_names = [desc[0] for desc in query.cursor.description]
    app.payouts = []
    [app.payouts.append(dict(zip(column_names, row)))
     for row in query.cursor.fetchall()]

def get_payouts_summary():
    """
    """
    conn = e.connect()
    try:
        query = conn.execute("SELECT * FROM payout_summary")
    except:
        sys.exit('Payout details table error')

    column_names = [desc[0] for desc in query.cursor.description]
    app.payouts_summary = []
    [app.payouts_summary.append(dict(zip(column_names, row)))
     for row in query.cursor.fetchall()]

def get_payouts_aggregate():
    """
    """
    conn = e.connect()
    try:
        query = conn.execute("SELECT * FROM payout_aggregate")
    except:
        sys.exit('Payout details table error')

    column_names = [desc[0] for desc in query.cursor.description]
    app.payouts_aggregate = []
    [app.payouts_aggregate.append(dict(zip(column_names, row)))
     for row in query.cursor.fetchall()]

def get_transactions():
    """
    Run at startup and add to app context the transactions
    """
    conn = e.connect()
    try:
        query = conn.execute("SELECT * FROM transactions")
    except:
        sys.exit('Payout details table error')
    column_names = [desc[0] for desc in query.cursor.description]
    app.transactions = []
    [app.transactions.append(dict(zip(column_names, row)))
     for row in query.cursor.fetchall()]

def get_db_data(queries):
    """
    """
    pass

def paginate_results(url, data, start, limit):
    """
    """
    count = len(data)
    if count < start:
        abort(404)

    obj = {}
    obj['start'] = start
    obj['limit'] = limit
    obj['count'] = count

    if start == 1:
        obj['previous'] = ''
    else:
        start_copy = max(1, start - limit)
        limit_copy = start - 1
        obj['previous'] = url + '?start=%d&limit=%d' % (start_copy, limit_copy)

    if start + limit > count:
        obj['next'] = ''
    else:
        start_copy = start + limit
        obj['next'] = url + '?start=%d&limit=%d' % (start_copy, limit)

    # finally extract result according to bounds
    obj['results'] = data[(start - 1):(start - 1 + limit)]
    return obj

@app.route('/api/payouts/', methods=['GET'])
@swag_from('payouts.yml')
def payouts():
    result = paginate_results('payouts/',
                               app.payouts,
                               int(request.args.get('start', 1)),
                               int(request.args.get('limit', 25)))
    result['success'] = True
    return jsonify(result)

@app.route('/api/pending/', methods=['GET'])
@swag_from('pending.yml')
def payouts_aggregate():
    """
    """
    sum_total = {}

    for d in app.payouts_aggregate:
        try:
            sum_total[d['address']] += d['rewards']
        except KeyError:
            sum_total[d['address']] = d['rewards']

    result = paginate_results('pending/',
                              [{'address':x, 'amount': round(y, 4)} for x,y
                                in sum_total.items()],
                              int(request.args.get('start', 1)),
                              int(request.args.get('limit', 25)))
    result['success'] = True
    return jsonify(result)

@app.route('/api/history/', methods=['GET'])
@swag_from('history.yml')
def payouts_aggregate_byaddress():
    sum_total = {}

    for d in app.payouts:
        try:
            sum_total[d['address']] += d['rewards']
        except KeyError:
            sum_total[d['address']] = d['rewards']

    result = paginate_results('history/',
                               [{'address':x, 'amount': round(y, 4)} for x,y
                                in sum_total.items()],
                               int(request.args.get('start', 1)),
                               int(request.args.get('limit', 500)))
    result['success'] = True
    return jsonify(result)


@app.route('/api/payouts/summary/', methods=['GET'])
@swag_from('payouts_summary.yml')
def payouts_summary():
    """
    """
    result = paginate_results('payouts/summary/',
                               app.payouts_summary,
                               int(request.args.get('start', 1)),
                               int(request.args.get('limit', 25)))
    result['success'] = True
    return jsonify(result)

@app.route('/api/transactions/', methods=['GET'])
@swag_from('transactions.yml')
def transactions():
    """
    """
    result = paginate_results('transactions/',
                               app.transactions,
                               int(request.args.get('start', 1)),
                               int(request.args.get('limit', 25)))
    result['success'] = True
    return jsonify(result)

@app.route('/')
def hello():
    """
    """
    data = [{"delegate":"slasheks"}]

    return render_template('index.html', data=data)

@app.route('/pending/')
def pending_view():
    """
    """
    get_payouts_aggregate()

    sum_total = {}

    for d in app.payouts_aggregate:
        try:
            sum_total[d['address']] += d['rewards']
        except KeyError:
            sum_total[d['address']] = d['rewards']

    data = [{'address':x, 'amount': round(y, 4)} for x,y in sum_total.items()]

    columns = [{
        "field": "address",
        "title": "address",
        "sortable": True,
    },
    {
        "field": "amount",
        "title": "pending amount",
        "sortable": True,
    }]

    return render_template('table.html',
                           title='Pending',
                           data=data,
                           columns=columns)

@app.route('/summary/')
def summary_view():
    """
    """
    get_payouts_summary()

    columns = [
    {
        "field": "timestamp",
        "title": "Timestamp...............",
        "sortable": True,
    },
    {
        "field": "dry_run",
        "title": "Dry Run",
        "sortable": True,
    },
    {
        "field": "forged",
        "title": "Forged",
        "sortable": True,
    },
    {
        "field": "forged_blk_count",
        "title": "Blocks",
        "sortable": True,
    },
    {
        "field": "forged_to_give",
        "title": "Accum",
        "sortable": True,
    },
    {
        "field": "percentage",
        "title": "Share",
        "sortable": True,
    },
    {
        "field": "vote_count",
        "title": "Vote Count",
        "sortable": True,
    },
    {
        "field": "Vote_count_filtered",
        "title": "Filtered",
        "sortable": True,
    },
    {
        "field": "weight",
        "title": "Weight",
        "sortable": True,
    },
    {
        "field": "weight_filtered",
        "title": "Filtered",
        "sortable": True,
    }]


    return render_template('table.html',
                           title='Summary',
                           data=app.payouts_summary,
                           columns=columns)

@app.route('/transactions/')
def transactions_view():
    """
    """
    get_transactions()

    new_tx_hack = []
    for transaction in app.transactions:
        new_tx_hack.append({
            'address': transaction['address'],
            'amount': round(transaction['amount'], 8),
            'message': transaction['message'],
            'success': transaction['success'],
            'timestamp': transaction['timestamp'],
            'txid': transaction['txid']
        })

    columns = [{
        "field": "address",
        "title": "address",
        "sortable": True,
    },
    {
        "field": "amount",
        "title": "amount paid",
        "sortable": True,
    },
    {
        "field": "message",
        "title": "message",
        "sortable": True,
    },
    {
        "field": "success",
        "title": "success",
        "sortable": True,
    },
    {
        "field": "timestamp",
        "title": "timestamp",
        "sortable": True,
    },
    {
        "field": "txid",
        "title": "Transaction ID",
        "sortable": True,
    }]

    return render_template('table.html',
                           title='Transactions',
                           data=new_tx_hack,
                           columns=columns)

if __name__ == '__main__':

    PARSER = argparse.ArgumentParser(description='')

    PARSER.add_argument('-o', '--option', dest='option',
                        action='store', help='')

    PARSER.add_argument('-l', '--listen', dest='listen',
                        action='store',default='127.0.0.1',
                        help='')

    PARSER.add_argument('-d', '--debug', dest='debug',
                        action='store_true', help='')

    ARGS = PARSER.parse_args()

    get_payouts()
    get_payouts_summary()
    get_payouts_aggregate()
    get_transactions()

    app.run(debug=ARGS.debug, host=ARGS.listen)


